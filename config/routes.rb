Alchemist::Application.routes.draw do
  devise_for :users

  root to: 'home#show'

  resources :ingredients, only: [:index]

  resources :recipes, only: [:index, :create]

end
