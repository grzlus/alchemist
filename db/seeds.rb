# encoding: utf-8

User.destroy_all
Ingredient.destroy_all
Recipe.destroy_all

User.create!( email: 'admin@example.com', password: 12345678, password_confirmation: 12345678 )

ingr = [:fire, :water, :air, :earth].collect do |e|
  Ingredient.create!( name: e.to_s.capitalize, category_id: 1, default: true )
end

ingr.combination( 2 ).each_with_index do |ing, i|
  result = Ingredient.new( name: "Ingredient #{ i }", category_id: 2 )
  recipe = Recipe.new
  recipe.substracts = ing.collect { |sub| Substract.new( ingredient_id: sub.id ) }
  recipe.ingredients << result
  recipe.save!
end
