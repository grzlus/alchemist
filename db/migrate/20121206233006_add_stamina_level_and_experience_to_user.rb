class AddStaminaLevelAndExperienceToUser < ActiveRecord::Migration
  def change
    add_column :users, :stamina, :integer, default: 100
    add_column :users, :level, :integer, default: 1
    add_column :users, :experience, :float, default: 0
  end
end
