class AddDefaultToIngredients < ActiveRecord::Migration
  def change
    add_column :ingredients, :default, :boolean
  end
end
