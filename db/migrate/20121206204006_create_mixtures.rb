class CreateMixtures < ActiveRecord::Migration
  def change
    create_table :mixtures do |t|
      t.text :substracts
      t.integer :user_id
      t.integer :recipe_id

      t.timestamps
    end
  end
end
