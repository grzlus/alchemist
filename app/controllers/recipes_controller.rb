class RecipesController < ApplicationController
  respond_to :html, :json

  def index
    @recipies = current_user.recipies
  end

  def create
    res = {}
    if params[:ingredient_ids] == nil
      res[:error] = t(:no_ingredients_givven)
    else
      elements = params[:ingredient_ids].map(&:last)
      known = current_user.ingredients.map( &:id ) 
      if elements.none? {|v| known.index v.to_i}
        res[:error] = t(:dont_know_one_or_more_ingredients)
      else
        if current_user.stamina > elements.count
          current_user.stamina -= elements.count
          res[:stamina] = current_user.stamina
          p elements
          rec = Recipe.with_substracts elements

          if rec.nil? or current_user.recipes.where( "recipe_id = ?", rec.id ).count > 0
            res[:message] = t(:not_discover)
          else
            new_ingredients = rec.ingredients.select { |ing| ( known.index ing.id).nil? }
            if new_ingredients.count > 0
              res[:ingredients] = new_ingredients
            end
            m = Mixture.new
            m.user = current_user
            m.recipe = rec
            m.substracts = elements
            m.save!

            res[:message] = t(:discovered, rec)

            current_user.experience += 5 * elements.count / ( current_user.level / 10.0 )
            if current_user.experience >= 100
              current_user.level += 1
              current_user.experience -= 100
              res[:level] = current_user.level
            end
            res[:experience] = current_user.experience
          end
        else
          res[:error] = t(:dont_have_stamina)
        end
      end
    end
    current_user.save!
    render json: res
  end
end
