class IngredientsController < ApplicationController

  respond_to :json

  def index
    render json: current_user.ingredients
  end

end
