class @GameGlue
  constructor: ( @useCase, @gui, @storage ) ->

    After( @useCase, 'showMainPage', => @storage.loadIngredients() )

    After( @storage, 'ingredientsLoaded', ( ingredients ) => @gui.renderPage( ingredients ) )

    After( @gui, 'recipeSubmitted', (ids) => @storage.submitRecipe( ids ) )

    After( @storage, 'errorOccured', (error) => @gui.showError( error ) )

    After( @storage, 'sendedRecipe', => @gui.unblockRecipe() )

    After( @storage, 'staminaChanged', (stamina) => @gui.staminaChanged( stamina ) )
    After( @storage, 'levelChanged', (stamina) => @gui.levelChanged( stamina ) )
    After( @storage, 'experienceChanged', (stamina) => @gui.experienceChanged( stamina ) )
    After( @storage, 'ingredientsDiscovered', ( ingrs ) => @gui.ingredientsDiscovered( ingrs ) )

    After( @storage, 'messageRecived', (message) => @gui.displayMessage( message ) )
