class @GameStorage

  constructor: ->

  loadIngredients: ->
    $.get 'ingredients.json', (data, e) =>
      if data.error?
        alert( data.error )
      else
        @ingredientsLoaded( data )

  ingredientsLoaded: ( data ) =>

  submitRecipe: (ids) ->

    callback = (data) =>

      if data.error?
        @errorOccured( data.error )

      if data.stamina?
        @staminaChanged( data.stamina )

      if data.level?
        @levelChanged( data.level )

      if data.experience?
        @experienceChanged( data.experience )

      if data.message?
        @messageRecived( data.message )

      if data.ingredients?
        @ingredientsDiscovered( data.ingredients )

      @sendedRecipe()

    pro_ids = {}
    ids.each (i,e) ->
      pro_ids[i] = e

    console.log pro_ids

    $.ajax
      type: "POST"
      url: '/recipes.json'

      data:
        ingredient_ids: pro_ids

      success: callback


  errorOccured: (error) ->
  staminaChanged: (stamina) ->
  levelChanged: (level) ->
  experienceChanged: (exp) ->
  messageRecived: (message) ->
  ingredientsDiscovered: (ingredients) ->
  sendedRecipe: ->
