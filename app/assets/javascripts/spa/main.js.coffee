class Game 
  constructor: ->
    useCase = new GameUseCase
    storage = new GameStorage

    gui = new GameGui

    glue = new GameGlue(useCase, gui, storage)

    useCase.showMainPage()

$ ->
  new Game
