class @GameGui
  constructor: ->
    @navbar = $('.top')
    @container = $('.main-container')

  renderPage: (ingredients) ->
    @navbar.append( HandlebarsTemplates["navbar"]({ categories: [ { name: "All", current: true }, { name: "Elements", id: 1 }, { name: "Other", id: 2 } ] }) )
    @container.append( HandlebarsTemplates["ingredients"]( { ingredients: ingredients }) )
    @container.append( HandlebarsTemplates["bars"]( $('body').data() ) )

    @bindItems()

  ingredientsDiscovered: ( ings ) ->
    cont = $('.ingredients')
    for ing in ings
      cont.append( HandlebarsTemplates['ingredient']( { ing: ing } ) )
      $('ul.ingredients a:last').draggable
        appendTo: "body",
        helper: "clone"


  bindItems: =>
    console.log "Binding items"
    gui = this
    $('ul.ingredient_select li a').click ->
      $(this).closest('ul').find('.active').removeClass('active')
      $(this).closest('li').addClass('active')
      cid = $(this).data('id')
      if cid != ''
        $('.ingredients li').hide()
        $(".ingredients li[data-category_id=#{ cid }]").stop().show()
      else
        $('.ingredients li').show()

    $('ul.ingredients a').draggable
      appendTo: "body",
      helper: "clone"

    $('.progress').popover()

    $('.bar').tooltip()

    $('.recipe').droppable
      drop: (e, ui) ->
        self = this

        if $('li', this).length < 8 and not $(self).hasClass? 'processing'
          hash =
            name: ui.draggable.text()
            id: ui.draggable.closest('li').data('id')


          obj = $( HandlebarsTemplates['recipe_item']( hash ) )

          obj.find('.close').click ->
            unless $(self).hasClass? 'processing'
              $(this).closest('li').hide 500, ->
                $(this).remove()

          $(this).find('.recipe-max').before( obj )

    $('#recipe-mix').click ->
      recipe = $('.recipe')
      unless recipe.hasClass? 'processing'
        recipe.addClass 'processing'
        ids = recipe.find('li > a').map (i, o) =>
          $(o).data('id')
        console.log ids
        gui.recipeSubmitted( ids )



  recipeSubmitted: (ids) ->

  unblockRecipe: ->
    $('.recipe').removeClass('processing')

  showError: (error) ->
    alert( error )

  displayMessage: (message) ->
    alert( message )

  staminaChanged: (stamina) ->
    $('.stamina .bar').css
      width: "#{ stamina }%"
    .attr 'data-original-title', "#{ stamina }%"

  levelChanged: (level) ->
    $('.level').text( level )

  experienceChanged: (exp) ->
    $('.exp .bar').css
      width: "#{ exp }%"
    .attr 'data-original-title', "#{ exp }%"

