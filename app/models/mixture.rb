class Mixture < ActiveRecord::Base
  attr_accessible :recipe_id, :substracts, :user_id

  validates_presence_of :user, :recipe

  belongs_to :recipe
  belongs_to :user

  serialize :substracts
end
