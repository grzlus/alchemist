class Recipe < ActiveRecord::Base
  # attr_accessible :title, :body

  has_many :substracts
  has_many :products
  has_many :mixtures

  has_many :ingredients, through: :products
  has_many :users, through: :mixtures

  validate :number_of_substracts
  validate :number_of_products

  # Return recipie that could be used with all given substracts
  def self.with_substracts ingredients
    if ingredients.is_a? Array
      ingredients.sort!.map!(&:to_i)
    else
      ingredients = [ ingredients ]
    end
    self.joins(:substracts).where( substracts: { ingredient_id: ingredients } ).detect { |recipe| recipe.substract_ids == ingredients }
  end

  def substract_ids
    substracts.select(:ingredient_id).order(:ingredient_id).map(&:ingredient_id)
  end

  protected

  def number_of_substracts
    errors.add(:substracts, :not_in_range) unless (1..5).include? substracts.size
  end

  def number_of_products
    errors.add(:products, :not_enough) if products.size == 0
  end
end
