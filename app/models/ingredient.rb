class Ingredient < ActiveRecord::Base
  attr_accessible :name, :category_id, :default

  CATEGORIES = [
    [1, :elements],
    [2, :other ]
  ]

  scope :defaults, lambda { where(default: true) }

  has_many :substracts
  has_many :products

  validates :name, presence: true
  validates :category_id, presence: true
end
