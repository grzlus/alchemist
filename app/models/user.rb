class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body
  
  has_many :mixtures
  has_many :recipes, through: :mixtures
  has_many :ingredients, through: :recipes, uniq: true

  def ingredients # UGLY
    ( super + Ingredient.defaults )
  end
end
